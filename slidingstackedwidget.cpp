/*
    SlidingStackedWidget
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include <QPropertyAnimation>
#include <QParallelAnimationGroup>

#include "slidingstackedwidget.h"

SlidingStackedWidget::SlidingStackedWidget(QWidget *parent)
    : QStackedWidget(parent),
      m_next(0),
      m_active(false),
      m_currPoint(0, 0)
{
}

SlidingStackedWidget::~SlidingStackedWidget()
{
}

void SlidingStackedWidget::slideNext()
{
    int now = currentIndex();
    if (now < count() - 1)
        slideWidget(now + 1, widget(now + 1), LEFTTORIGHT);
}

void SlidingStackedWidget::slidePrevious()
{
    int now = currentIndex();
    if (now > 0)
        slideWidget(now - 1, widget(now - 1), RIGHTTOLEFT);
}

void SlidingStackedWidget::slideInto(int index, SlidingDirection direction)
{
    if (index < 0 || index >= count())
        return;

    slideWidget(index, widget(index), direction);
}

void SlidingStackedWidget::slideWidget(int index, QWidget *wid, SlidingDirection direction)
{
    if (m_active)
        return;
    else
        m_active = true;

    if (currentIndex() == index) {
        m_active = false;
        return;
    }

    QWidget *nowWid = currentWidget();

    int offsetX = 0;
    int offsetY = 0;
    int widW = frameRect().width();
    int widH = frameRect().height();

    wid->setGeometry(offsetX, offsetY, widW, widH);

    if (direction == LEFTTORIGHT) {
        offsetX = widW;
    } else if (direction == RIGHTTOLEFT) {
        offsetX = -widW;
    }

    QPoint nextPoint = wid->pos();
    QPoint currPoint = nowWid->pos();

    m_currPoint = currPoint;

    wid->move(nextPoint.x() - offsetX, nextPoint.y() - offsetY);
    wid->show();
    wid->raise();

    QPropertyAnimation *animNow = new QPropertyAnimation(nowWid, "pos");
    animNow->setDuration(150);
    animNow->setEasingCurve(QEasingCurve::Linear);
    animNow->setStartValue(QPoint(currPoint.x(), currPoint.y()));
    animNow->setEndValue(QPoint(offsetX + currPoint.x(), offsetY + currPoint.y()));

    QPropertyAnimation *animNext = new QPropertyAnimation(wid, "pos");
    animNext->setDuration(150);
    animNext->setEasingCurve(QEasingCurve::Linear);
    animNext->setStartValue(QPoint(-offsetX + nextPoint.x(), offsetY + nextPoint.y()));
    animNext->setEndValue(QPoint(nextPoint.x(), nextPoint.y()));

    QParallelAnimationGroup *animGroup = new QParallelAnimationGroup(this);
    animGroup->addAnimation(animNow);
    animGroup->addAnimation(animNext);

    connect(animGroup, &QParallelAnimationGroup::finished, this, &SlidingStackedWidget::animationDone);

    m_next = index;
    m_active = true;

    animGroup->start(QAbstractAnimation::DeleteWhenStopped);
}

void SlidingStackedWidget::animationDone()
{
    int curr = currentIndex();
    setCurrentIndex(m_next);
    widget(curr)->hide();
    widget(curr)->move(m_currPoint);
    m_active = false;
    emit animationFinished();
}
